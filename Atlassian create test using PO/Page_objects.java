package create_with_PO;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

public class Page_objects {
	public static void main(String[] args) {
        // Create a new instance of a driver
        WebDriver driver = new FirefoxDriver();

        // Navigate to the right place
        driver.get("https://davids.atlassian.net/");

        //Instantiate the elements
        Create_new page = PageFactory.initElements(driver, Create_new.class);

        // Log into the website.
        page.user("davids@cqs.co.za");
        page.password("Amvantage_87");
        
        //Click on the create button
        page.create.click();
        
        //Allow for the page to load
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        
        //Click on the template
        page.template.click();
        
        //Click on the create template button
        page.createTemplate.click();
        
        //Insert text into the heading and the body
        page.heading("This is the new heading for the blog");
                
        //Click on the publish button
        page.publish.click();
      
                
}
}