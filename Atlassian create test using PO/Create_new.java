package create_with_PO;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Create_new {
	 
		//The username
			private WebElement username;
			public void user(String text) {
				username.sendKeys(text);
			}
			
		//The password
			private WebElement password;
			public void password(String text) {
				password.sendKeys(text);
				password.submit();
			}
		//Click the create button
			@FindBy(id = "create-page-button")
		    WebElement create;
		
		//Click on the Blog post
			@FindBy(xpath=("//span[contains(@class,'template-preview') and contains(@class, 'icon-blogpost-large')]"))
		    WebElement template;
			
		//Click on the create button for the template	
			@FindBy(xpath=("//button[contains(@class,'create-dialog-create-button') and contains(@class, 'aui-button-primary')]"))
			WebElement createTemplate;
			
		//The title and body	
			@FindBy(id="content-title")
			private WebElement heading;
			public void heading(String text) {
				heading.sendKeys(text, Keys.TAB, "This is the new text for the body");
			}
			
		//Click on the publish button
			@FindBy(id="rte-button-publish")
			WebElement publish;
			
			
			
}